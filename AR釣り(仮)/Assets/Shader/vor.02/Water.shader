﻿Shader "Custom/Water"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Alpha("Alpha",Range(0,4)) = 1.5
		_Speed("Speed",Range(10,100)) = 50
		_Pointx("Pointx",Float) = 0
		_Pointy("Pointy",Float) = 0
		_Pointz("Pointz",Float) = 0
		_WaveWidth("WaveWidth",Float) = 0
	}
		SubShader
	{
		// Queueで描画順を設定出来る
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		LOD 200


		Pass{
			ZWrite ON
			ColorMask 0
		}

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha:fade
		#pragma target 3.0


		struct Input
		{
			float3 worldNormal;	// 法線
			float3 viewDir;		// カメラの視線ベクトル
			float3 worldPos;

		};

		half _Glossiness;
		half _Metallic;
		half _Alpha;
		fixed4 _Color;
		fixed _Speed;

		float _Pointx;
		float _Pointy;
		float _Pointz;
		float _WaveWidth;

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			float dist = distance(float3(_Pointx, _WaveWidth, _Pointz), IN.worldPos);
			float val = abs(sin(dist * 3.0 - _Time * _Speed));

			o.Albedo = _Color;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			float alpha = 1 - (abs(dot(IN.viewDir, IN.worldNormal)));
			//o.Alpha = alpha * _Alpha;

			if (_Pointy < -5.5 && val > 0.98) {
				o.Alpha = 1;
			}
			else {
				o.Alpha = alpha * _Alpha;
			}
		}
		ENDCG
    }
    FallBack "Diffuse"
}
