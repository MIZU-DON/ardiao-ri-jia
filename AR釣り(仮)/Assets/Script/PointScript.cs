﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointScript : MonoBehaviour
{
    public GameObject Point;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Renderer>().material.SetFloat("_Pointx", Point.transform.position.x);
        GetComponent<Renderer>().material.SetFloat("_Pointy", Point.transform.position.y);
        GetComponent<Renderer>().material.SetFloat("_Pointz", Point.transform.position.z);
    }
}