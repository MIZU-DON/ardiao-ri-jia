﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class closeScript : MonoBehaviour
{
    public GameObject Window;

    // Start is called before the first frame update
    void Start()
    {
        Window.SetActive(true);
    }

    //　ウィンドウを閉じる
    public void CloseButtonDown()
    {
        Window.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
