﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicChangeMateColor : MonoBehaviour
{
    private Material TargetMaterial;
    [SerializeField] Color setColor;

    // Start is called before the first frame update
    void Start()
    {
        TargetMaterial = GetComponent<Renderer>().material;
        TargetMaterial.color = setColor;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
