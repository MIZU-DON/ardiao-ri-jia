﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class radioScript : MonoBehaviour
{
    [SerializeField] Transform btns;
    void Start()
    {
        SetBtns();
    }
    public void PushBtn(int n)
    {
        // int型のデータnを"radio"に保存
        PlayerPrefs.SetInt("radio", n);
        SetBtns();
    }
    void SetBtns()
    {
        // "radio"からデータを読み込む
        int r = PlayerPrefs.GetInt("radio"), cnt = 0;

        // ボタン数ループして該当ボタン以外を無効にする
        foreach (Transform b in btns) { b.GetComponent<Button>().interactable = (cnt != r); cnt++; }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
