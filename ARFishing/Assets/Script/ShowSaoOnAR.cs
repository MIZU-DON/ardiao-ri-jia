﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowSaoOnAR : MonoBehaviour
{
    [SerializeField] GameObject prefab;

    Quaternion qua;
    Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(prefab, pos, qua);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
