﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuScript : MonoBehaviour
{

    public GameObject menu;

    public GameObject Operation;

    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
        Operation.SetActive(false);
    }

    // メニューボタン
    public void MenuButtonDown()
    {
        menu.SetActive(true);
    }

    //　タイトルバック
    public void TitleBackButtonDown()
    {
        SceneManager.LoadScene("TitleScene");
    }

    //　操作設定
    public void OperationButtonDown()
    {
        Operation.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
