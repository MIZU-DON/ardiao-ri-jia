﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FishingScript : MonoBehaviour
{
    private Animator animator;          // 釣りアニメーター
    private int flg = 0;                // 釣りアニメーションフラグ
    public int ChangeNam;

    radioScript radio;                  // 操作変更スクリプト

    public GameObject Button;           // 釣りボタンオブジェクト

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("flg1", false);
    }

    // 釣り！ボタン
    public void ButtonDown()
    {
        flg++;
    }

    // 釣り！フリック
    private Vector3 touchStartPos;      // タッチした座標
    private Vector3 touchEndPos;        // 離した座標

    void Flick()
    {
        
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            touchStartPos = new Vector3(Input.mousePosition.x,
                                        Input.mousePosition.y,
                                        Input.mousePosition.z);
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            touchEndPos = new Vector3(Input.mousePosition.x,
                                      Input.mousePosition.y,
                                      Input.mousePosition.z);

            float directionY = touchEndPos.y - touchStartPos.y;

            // 上フリック処理
            if (30 < directionY)
            {
                flg = 1;
            }
            // 下フリック処理
            else if (-30 > directionY)
            {
                flg = 2;
            }
            // その他
            else
            {
                flg = 0;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        // 釣りアニメーション変更
        switch (flg){
            case 0:
                break;
            case 1:
                animator.SetBool("flg1", true);
                break;
            case 2:
                animator.SetBool("flg1", false);
                flg = 0;
                break;
            default:
                break;
        }

        // 操作変更
        ChangeNam = PlayerPrefs.GetInt("radio");
        switch (ChangeNam)
        {
            // ボタン操作
            case 0:
                Button.SetActive(true);
                break;
            // フリック操作
            case 1:
                Button.SetActive(false);
                Flick();
                break;
            // ジャイロ操作
            case 2:
                Button.SetActive(false);
                break;
            default:
                break;
        }

        // シーンリセット
        //if (Input.GetKey(KeyCode.Z))
        //{
        //    SceneManager.LoadScene("GameScene");
        //}

    }
}
