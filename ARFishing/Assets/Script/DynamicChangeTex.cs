﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicChangeTex : MonoBehaviour
{
    [SerializeField]private Texture texture;
    private Material TargetMaterial;

    // Start is called before the first frame update
    void Start()
    {
        TargetMaterial = GetComponent<Renderer>().material;
        TargetMaterial.mainTexture = texture;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
